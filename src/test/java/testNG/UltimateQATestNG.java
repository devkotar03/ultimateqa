package testNG;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.BigPages;
import pages.FakeLandingPage;
import pages.FakePricingPage;
import pages.FillOutForm;
import pages.InteractionPage;
import pages.LearnToAutomate;
import pages.SignUp;

public class UltimateQATestNG {
	static WebDriver driver;
	public static String browserName = null;

	@BeforeTest
	public void setupTest() {
		Propertiesfile.getProperties();
		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./libs/geckodriver");
			driver = new FirefoxDriver();
		}
	}

	public static void navigateToWebPage() {
		driver.get("http://www.ultimateqa.com/automation");
		System.out.println("The Browser is Invoked");
	}

	@Test(priority = 0)
	public static void LoginAutomation() throws InterruptedException {
		navigateToWebPage();
		SignUp obj = new SignUp(driver);
		obj.login("rajandevkota1@gmail.com", "Texas123");
	}

	@Test(priority = 1)
	public static void Filloutform() throws InterruptedException {
		FillOutForm obj = new FillOutForm(driver);
		obj.Fillform();
		obj.Name("Rajan");
		obj.Message("I live in Irving");
		obj.submit();
		obj.Anothername("Rajan Devkota");
		obj.Anothermessage("I like playing Football");
		obj.parse();
		obj.SubmitAgain();
	}

	@Test(priority = 2)
	public static void FakePricingPage() {
		FakePricingPage obj = new FakePricingPage(driver);
		obj.Fakepricing();
		obj.Basicpurchase();
	}

	@Test(priority = 3)
	public static void FakeLandingPage() {
		FakeLandingPage obj = new FakeLandingPage(driver);
		obj.Fakelandingpage();
		obj.courses();
		obj.naviagtevideo();
		obj.enrollincourse();
	}

	@Test(priority = 4)
	public static void LearnToAutomate() {
		LearnToAutomate obj = new LearnToAutomate(driver);
		obj.Learntoautomate();
		obj.Firstname("Rajan");
		obj.Submit();
	}

	@Test(priority = 5)
	public static void InteractionPage() throws InterruptedException {
		InteractionPage obj = new InteractionPage(driver);
		obj.InteractionPage();
		obj.Buttons("Audi");
	}

	@Test(priority = 6)
	public static void BigPages() {
		BigPages obj = new BigPages(driver);
		obj.Bigpage();
		obj.Login("devkotar03", "Texas123");
	}

	@AfterTest
	public void tearDownTest() {
		driver.close();
		driver.quit();
		System.out.println("Test Completed Successfully");

	}

	@AfterSuite
	public void tearDown() {

	}

}
