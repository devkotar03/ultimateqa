package testNG;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import testNG.UltimateQATestNG;


public class Propertiesfile {
	static Properties prop = new Properties();
	
	//Get the project path
	static String projectPath = System.getProperty("user.dir");
	public static void main(String[] args) {
		getProperties();
		setProperties();
		getProperties();
	}
	
	public static void getProperties() {
		try {
InputStream input = new FileInputStream(projectPath + "/src/test/java/testNG/config.properties");
prop.load(input);
//4 Get the values from the properties filr
		String browser = prop.getProperty("browser");
		//printing the check if correct value was received
		System.out.println(browser + " was Invoked");
		//Set the browser for the particular test case
		UltimateQATestNG.browserName= browser;
		
	}catch(Exception e) {
		System.out.println(e.getMessage());
		System.out.println(e.getCause());
		e.printStackTrace();
	}
}	
public static void setProperties() {
	try {
	//set data to properties file
	//create object to class output stream
	OutputStream output = new FileOutputStream (projectPath + "/src/test/java/testNG/config.properties");
	//setting the values
	prop.setProperty("browser", "chrome");
	//prop.setProperty("Result", "Pass");
	prop.store(output, null);
	}catch(Exception e) {
		System.out.println(e.getMessage());
		System.out.println(e.getCause());
		e.printStackTrace();
	}

	}	
	}