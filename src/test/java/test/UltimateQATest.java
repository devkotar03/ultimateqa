package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Interaction;

import pages.BigPages;
import pages.FakeLandingPage;
import pages.FakePricingPage;
import pages.FillOutForm;
import pages.InteractionPage;
import pages.LearnToAutomate;
import pages.SignUp;

public class UltimateQATest {
	public static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		invokeBrowser();
		LoginAutomation();
		Filloutform();
		FakePricingPage();
		FakeLandingPage();
		LearnToAutomate();
		InteractionPage();
		BigPages();
	}

	public static void invokeBrowser() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		driver = new ChromeDriver();
		Thread.sleep(1000);
		driver.get("http://www.ultimateqa.com/automation");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public static void LoginAutomation() throws InterruptedException {
		SignUp obj = new SignUp(driver);
		obj.login("rajandevkota1@gmail.com", "Texas123");	
	}
	
	
	public static void Filloutform() throws InterruptedException {
		FillOutForm obj = new FillOutForm(driver);
		obj.Fillform();
		obj.Name("Rajan");
		obj.Message("I live in Irving");
		obj.submit();
		obj.Anothername("Rajan Devkota");
		obj.Anothermessage("I like playing Football");
		obj.parse();
		obj.SubmitAgain();
	}
	public static void FakePricingPage(){
		FakePricingPage obj = new FakePricingPage(driver);
		obj.Fakepricing();
		obj.Basicpurchase();
	}
	public static void FakeLandingPage() {
		FakeLandingPage obj = new FakeLandingPage(driver);
		obj.Fakelandingpage();
		obj.courses();
		obj.naviagtevideo();
		obj.enrollincourse();
	}
	public static void LearnToAutomate() {
		LearnToAutomate obj = new LearnToAutomate(driver);
		obj.Learntoautomate();
		obj.Firstname("Rajan");
		obj.Submit();
	}
	public static void InteractionPage() throws InterruptedException {
	InteractionPage obj = new InteractionPage(driver);
	obj.InteractionPage();
	obj.Buttons("Audi");
	}
	
	public static void BigPages() {
	BigPages obj = new BigPages(driver);
	obj.Bigpage();
	obj.Login("devkotar03", "Texas123");
}
}