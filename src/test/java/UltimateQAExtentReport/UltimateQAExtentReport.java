package UltimateQAExtentReport;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import pages.BigPages;
import pages.FakeLandingPage;
import pages.FakePricingPage;
import pages.FillOutForm;
import pages.InteractionPage;
import pages.LearnToAutomate;
import pages.SignUp;

public class UltimateQAExtentReport {
	// Global Variables
	ExtentHtmlReporter htmlReporter;
	static ExtentReports extent;
	ExtentTest test;
	static WebDriver driver;

	@BeforeSuite
	public void setup() {
		// creating extend report and attach the report
		htmlReporter = new ExtentHtmlReporter("UltimateQA.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}

	@BeforeTest
	public void setUpTestInvokeBrowser() {
		// setting system path
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		// creating the driver obj
		driver = new ChromeDriver();

	}

	public void navigateToWebPage() {
		driver.get("http://www.ultimateqa.com/automation");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		ExtentTest isBrowserOpened = extent.createTest("Invoke Browser","This test ensures that the browser is invoked");
		isBrowserOpened.pass("Browser was invoked as Expected");

	}

	@Test(priority = 0)
	public void LoginAutomation() throws InterruptedException {
		navigateToWebPage();
		ExtentTest tests = extent.createTest("Login Automation page ", "This Page ensures it is running");
		tests.log(Status.INFO, "Execute the Page");
		
		ExtentTest test = extent.createTest("Login Automation page ", "This Page ensures it is running");
		test.log(Status.INFO, "Execute the Page");
		SignUp obj = new SignUp(driver);
		obj.login("rajandevkota1@gmail.com", "Texas123");
		test.pass("Email, Password Validated");
	}

	@Test(priority = 1)
	public void Filloutform() throws InterruptedException {
		ExtentTest test1 = extent.createTest("FilloutForm Page", "This Page ensures it is running");
		test1.log(Status.INFO, "Execute the Page");
		FillOutForm obj = new FillOutForm(driver);
		obj.Fillform();
		test1.pass("Filloutform link and clicked");
		obj.Name("Rajan");
		test1.pass("Name");
		obj.Message("I live in Irving");
		test1.pass("Message");
		obj.submit();
		test1.pass("Submit button is clicked");
		obj.Anothername("Rajan Devkota");
		test1.pass("Enter Name Again");
		obj.Anothermessage("I like playing Football");
		test1.pass("Enter another message");
		obj.parse();
		test1.pass("Pass the number");
		obj.SubmitAgain();
		test1.pass("Click Submit");
	}

	@Test(priority = 2)
	public void FakePricingPage() {
		ExtentTest test2 = extent.createTest("FakePricing Page", "This Page ensures it is running");
		test2.log(Status.INFO, "Execute the Page");
		FakePricingPage obj = new FakePricingPage(driver);
		obj.Fakepricing();
		test2.pass("Fakepricing linked is clicked");
		obj.Basicpurchase();
		test2.pass("Basic purchase button is clicked");
	}

	@Test(priority = 3)
	public void FakeLandingPage() {
		ExtentTest test3 = extent.createTest("Fakelanding Page", "This Page ensures it is running");
		test3.log(Status.INFO, "Execute the Page");
		FakeLandingPage obj = new FakeLandingPage(driver);
		obj.Fakelandingpage();
		test3.pass("Fakelanding linked in clicked");
		obj.courses();
		test3.pass("Course is selected");
		obj.naviagtevideo();
		test3.pass("The video link is clicked");
		obj.enrollincourse();
		test3.pass("Successfully enrolled in course");
	}

	@Test(priority = 4)
	public void LearnToAutomate() {
		ExtentTest test4 = extent.createTest("Learn To Automate Page", "This Page ensures it is running");
		test4.log(Status.INFO, "Execute the Page");
		LearnToAutomate obj = new LearnToAutomate(driver);
		obj.Learntoautomate();
		test4.pass("Learn to automate linked is clicked");
		obj.Firstname("Rajan");
		test4.pass("Enter your First name");
		obj.Submit();
		test4.pass("Submit button is clicked");
	}

	@Test(priority = 5)
	public void InteractionPage() throws InterruptedException {
		ExtentTest test5 = extent.createTest("Interaction Page", "This Page ensures it is running");
		test5.log(Status.INFO, "Execute the Page");
		InteractionPage obj = new InteractionPage(driver);
		obj.InteractionPage();
		test5.pass("Interaction page linked is clicked");
		obj.Buttons("Audi");
		test5.pass("valaidate and selected male, I have a car, Audi, Tab1, Tab2");
	}

	@Test(priority = 6)
	public void BigPages() {
		ExtentTest test6 = extent.createTest("Big Pages", "This Page ensures it is running");
		test6.log(Status.INFO, "Execute the Page");
		BigPages obj = new BigPages(driver);
		obj.Bigpage();
		test6.pass("Bigpage linked is clicked");
		obj.Login("devkotar03", "Texas123");
		test6.pass("Username, password is entered");
	}

	@AfterTest
	public void tearDownTest() {
		driver.close();
		driver.quit();
		ExtentTest isBrowserClosed = extent.createTest("Terminate Browser", "The browser is being closed");

		isBrowserClosed.pass("Browser is closed");

	}

	@AfterSuite
	public void tearDown() {

		// calling the flush writes everything to the log file

		extent.flush();
	}

}
