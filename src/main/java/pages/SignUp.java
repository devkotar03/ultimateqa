package pages;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SignUp {
	WebDriver driver;

	@FindBy(how = How.XPATH, xpath = "//a[contains(text(),'Login automation')]")
	public WebElement LoginFiled;

	@FindBy(how = How.XPATH, xpath = "//input[@type='email']")
	public WebElement EmailField;
	
	@FindBy(how = How.XPATH, xpath = "//input[@id='user[password]']")
	public WebElement PasswordField;
	
	
	

	public SignUp(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	public void login(String email, String password) throws InterruptedException {
		LoginFiled.click();
		TimeUnit.SECONDS.sleep(3);
		EmailField.sendKeys(email);
		PasswordField.sendKeys(password);
		Screenshot.captureScreenShots(driver, "SignUp");
	driver.navigate().to("https://ultimateqa.com/automation");
}
	}
	
	