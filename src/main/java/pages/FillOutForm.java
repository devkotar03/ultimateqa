package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FillOutForm {
	WebDriver driver;
	WebElement element;
	By filloutforms = By.xpath("//div[@id=\"et-boc\"]/div/div/div[2]/div/div[1]/div/div/div/div/ul/li[4]/a");
	By Name = By.xpath("//input[@id='et_pb_contact_name_0']");
	By Message =By.xpath("//textarea[@id='et_pb_contact_message_0']");
	By Submit =By.xpath("//div[@id=\"et_pb_contact_form_0\"]/div[2]/form/div/button");
	By AnotherName= By.xpath("//input[@id='et_pb_contact_name_1']");
	By Anothermessage =By.xpath("//textarea[@id='et_pb_contact_message_1']");
	By ParseQuestion =By.xpath("//span[@class='et_pb_contact_captcha_question']");
	By ParseAnswer =By.xpath("//input[@name='et_pb_contact_captcha_1']");
	By SubmitAgain =By.xpath("//div[@id=\"et_pb_contact_form_1\"]/div[2]/form/div/button");
	public FillOutForm(WebDriver driver) {

		this.driver = driver;

	}

	public void Fillform() {
		driver.findElement(filloutforms).click();
	}
	
	public void Name(String name) {
		driver.findElement(Name).sendKeys(name);
	}
	
    public void Message(String message) {
    	driver.findElement(Message).sendKeys(message);
    }
    public void submit() throws InterruptedException {
    		driver.findElement(Submit).click(); 
    		TimeUnit.SECONDS.sleep(2);
    	
    }
    
    public void Anothername(String othername) {
    	driver.findElement(AnotherName).sendKeys(othername);
    }
    public void Anothermessage(String newmessage) {
    	driver.findElement(Anothermessage).sendKeys(newmessage);
    	
    }
   public void parse() throws InterruptedException {
	 //Getting the text from page and generate sum
	   String num1= driver.findElement(ParseQuestion).getText().trim();
	   String removespace = num1.replaceAll("\\s+", "");
	   //Get two number
	   String[]parts = removespace.split("\\+");
		  String part1 = parts[0];
		  String part2 = parts[1];
		  String[]parts1 = part2.split("\\=");
		  String Lastpart = parts1[0];
		  int summation = Integer.parseInt(part1)+ Integer.parseInt(Lastpart);
		  String s = String.valueOf(summation);
		  driver.findElement(ParseAnswer).sendKeys(s);
		  System.out.println("The Values are :" + s);
		  TimeUnit.SECONDS.sleep(2);
			 
   }
    
    public void SubmitAgain() throws InterruptedException {
    	TimeUnit.SECONDS.sleep(1);
    	driver.findElement(SubmitAgain).click();
    	TimeUnit.SECONDS.sleep(1);
    	 driver.navigate().to("https://ultimateqa.com/automation");
    }
    
    }

