package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FakeLandingPage {
	WebDriver driver;
	WebElement element;
	By Fakelandingpage = By.xpath("//a[contains(text(),'Fake Landing Page')]");
	By Courses =By.xpath("//li[@id=\"menu-item-504\"]/a");
	By Implicitexplict =By.xpath("//div[1]//article[1]//ul[1]//li[2]//a[1]//header[1]//img[1]");
	By Enroll =By.xpath("//a[contains(text(),'Enroll for free')]");
	
	public FakeLandingPage(WebDriver driver) {

		this.driver = driver;

	}

	public void Fakelandingpage() {
		driver.findElement(Fakelandingpage).click();
	}
	
	public void courses() {
		driver.findElement(Courses).click();
	}
	public void naviagtevideo() {
		driver.findElement(Implicitexplict).click();
	}
	public void enrollincourse() {
		driver.findElement(Enroll).click();
		 driver.navigate().to("https://ultimateqa.com/automation");
	}
	
}
