package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InteractionPage {
	WebDriver driver;

	@FindBy(how = How.XPATH, xpath = "//a[contains(text(),'Interactions with simple elements')]")
	public WebElement Interaction;
	
	@FindBy(how=How.XPATH,xpath="//div[@id=\"et-boc\"]/div/div/div[3]/div/div[1]/div[7]/div/div/div/form/input[1]")
	public WebElement RadioBtn;

	@FindBy(how=How.XPATH,xpath ="//div[@id=\"et-boc\"]/div/div/div[3]/div/div[1]/div[8]/div/div/div/form/input[2]")
	public WebElement CheckBox;
	
	@FindBy(how=How.XPATH, xpath="//div[@id=\"et-boc\"]/div/div/div[3]/div/div[1]/div[9]/div/div/div/select")
	public WebElement Dropdown;
	
	@FindBy(how=How.XPATH,xpath= "//a[contains(text(),'Tab 1')]")
	public WebElement Tab1;
	@FindBy(how=How.XPATH,xpath= "//a[contains(text(),'Tab 2')]")
	public WebElement Tab2;
	
	
	public InteractionPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	public void InteractionPage() {
		Interaction.click();
			
		}
	
public void Buttons(String drop) throws InterruptedException {
	TimeUnit.SECONDS.sleep(3);
	RadioBtn.click();
	TimeUnit.SECONDS.sleep(3);
	CheckBox.click();
	TimeUnit.SECONDS.sleep(3);
	Select Drop = new Select(Dropdown);
	Drop.selectByVisibleText(drop);
	TimeUnit.SECONDS.sleep(3);
	Tab1.click();
	System.out.println("tab 1 content");
	Tab2.click();
	System.out.println("Tab 2 content");
	driver.navigate().to("https://ultimateqa.com/automation");	
}



}
