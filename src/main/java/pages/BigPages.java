package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class BigPages {

	WebDriver driver;

	@FindBy(how = How.XPATH, xpath = "//div[@id=\"et-boc\"]/div/div/div[2]/div/div[1]/div/div/div/div/ul/li[1]/a")
	public WebElement BigpageHomepage;

	@FindBy(how=How.XPATH, xpath= "//input[@id='user_login_5e625bdbcb7fb']")
	public WebElement UsernameField;
	
	@FindBy(how=How.XPATH, xpath= "//input[@id='user_pass_5e625bdbcb7fb']")
	public WebElement PasswordField;
	
	
	
	public BigPages(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}
	
	public void Bigpage() {
		BigpageHomepage.click();
		
	}
public void Login(String username, String password) {
	UsernameField.sendKeys(username);
	PasswordField.sendKeys(password);
	driver.navigate().to("https://ultimateqa.com/automation");
	System.out.println("The page run Successfully");
}
}

