package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FakePricingPage {
	WebDriver driver;
	WebElement element;
	By fakepricingpage = By.xpath("//div[@id=\"et-boc\"]/div/div/div[2]/div/div[1]/div/div/div/div/ul/li[3]/a");
	By Basic =By.xpath("//div[@id=\"et-boc\"]/div/div/div[1]/div[2]/div[2]/div/div/div/div[4]/a");
	
	public FakePricingPage(WebDriver driver) {

		this.driver = driver;

	}

	public void Fakepricing() {
		driver.findElement(fakepricingpage).click();
		
	}
	public void Basicpurchase() {
		driver.findElement(Basic).click();
		 driver.navigate().to("https://ultimateqa.com/automation");
	}
}

