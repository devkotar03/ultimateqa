package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LearnToAutomate {
	WebDriver driver;
	WebElement element;
	By Howtoautomate = By.xpath("//div[@id=\"et-boc\"]/div/div/div[2]/div/div[1]/div/div/div/div/ul/li[5]/a");
	By Fname= By.xpath("//input[@name='firstname']");
	By Submit =By.xpath("//input[@id='submitForm']");

	public LearnToAutomate(WebDriver driver) {

		this.driver = driver;

	}
	public void Learntoautomate() {
		driver.findElement(Howtoautomate).click();
	}
	public void Firstname(String automate) {
		driver.findElement(Fname).sendKeys(automate);
		
	}
	public void Submit() {
		driver.findElement(Submit).click();
		driver.navigate().to("https://ultimateqa.com/automation");
	}
}
